<?php
include 'db.php';

$email = $conn->real_escape_string(strtolower($_REQUEST['email']));
$pass = $conn->real_escape_string($_REQUEST['pass']);

session_start();
$sql = "SELECT Email FROM Tab_Reg WHERE Email='" . $email . "' AND id='" . $conn->real_escape_string($_SESSION["id"]) . "'";
$result1 = $conn->query($sql) or die ('Something has gone wrong, try again later');

$sql = "SELECT Email FROM Tab_Reg WHERE Email='" . $email . "'";
$result2 = $conn->query($sql) or die ('Something has gone wrong, try again later');

if($result1->num_rows != $result2->num_rows){
	header("location:account.php?e=2");
	exit();
}

if($pass != ""){
	$sql = "SELECT T FROM Tab_Reg WHERE id = '" . $conn->real_escape_string($_SESSION["id"]) . "'";
	$result = $conn->query($sql);

	if(!$result){
		die ('Something has gone wrong, try again later');
	}

	if($result->num_rows > 0){
		$row = $result->fetch_assoc();
		if(password_verify($pass, $row['T'])){
			$pass = $row['T'];
		}else{
			$pass = password_hash($pass, PASSWORD_DEFAULT);
		}
	}else{
		header("location:login.php?e=1");
		exit();
	}

	$sql = "UPDATE Tab_Reg SET Email='" . $email . "', T='" . $pass . "' WHERE id = '" . $conn->real_escape_string($_SESSION["id"]) . "'";
}else{
	$sql = "UPDATE Tab_Reg SET Email='" . $email . "' WHERE id = '" . $conn->real_escape_string($_SESSION["id"]) . "'";
}

$conn->query($sql) or die(mysqli_error($conn));
header("location:account.php");
exit();
?>