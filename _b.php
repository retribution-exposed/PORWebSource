<?php include 'db.php';
header('Content-Type: application/json');
$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
$match = '149.56.99.2';

if (substr($ip, 0, strlen($match)) !== $match) {
        echo json_encode(array('status' => 'error', 'code' => '1', 'message' => 'RoT RoT Gang'));
        exit(0);
}

if (!isset($_POST['account']) || !isset($_POST['hours'])) {
	echo json_encode(array('status' => 'error', 'code' => '0', 'message' => 'What do you mean?'));
	exit(0);
}

if ($secrettoken !== $_POST['secrettoken'] || $_SERVER['HTTP_USER_AGENT'] !== 'POR-ClientServicesManagerUD') {
	echo json_encode(array('status' => 'error', 'code' => '1', 'message' => 'Who are you?'));
	exit(0);
}

$account = $conn->real_escape_string($_POST['account']);
$hours = $_POST['hours'];

if (!is_numeric($hours)) {
  echo json_encode(array('status' => 'error', 'code' => '2', 'message' => 'Hours must be numeric!'));
  exit(0);
}

$hours = (int) $hours;

if ($hours === 0) {
  $time = 1;
  $message = 'forever';
} else {
  $time = time() + (3600 * $hours);
  $message = $hours . ' hours';
}

$sql = "UPDATE Tab_Reg SET BannedUntil='" . $time . "', Temp=NULL, TempExpire=NULL WHERE UserName='" . $account . "'";
$conn->query($sql);

if ($hours === 0) {
  
  $sql = "SELECT Address FROM Tab_Reg WHERE UserName='". $account ."'";
  $result = $conn->query($sql);
  $row = $result->fetch_assoc();
  $getAddr = $row['Address'];

  if ($address == "149.56.99.235") {} else {
    $sql = "INSERT INTO Tab_BannedIP (id, Address) VALUES (NULL, '". $getAddr ."')";
    $conn->query($sql);
  }
}

echo json_encode(array('status' => 'success', 'code' => '3', 'message' => 'Banned user ' . $account . ' for ' . $message . '!'));
?>
