<?php
include 'db.php';

function getUserIP() {
	$client  = $_SERVER['HTTP_CLIENT_IP'];
	$forward = $_SERVER['HTTP_X_FORWARDED_FOR'];
	$remote  = $_SERVER['REMOTE_ADDR'];
	if(filter_var($client, FILTER_VALIDATE_IP)) {
		$ip = $client;
	} else if(filter_var($forward, FILTER_VALIDATE_IP))  {
		$ip = $forward;
	} else {
		$ip = $remote;
	}
	
	return $ip;
}

$user = $conn->real_escape_string($_REQUEST['user']);
$pass = $conn->real_escape_string($_REQUEST['pass']);
$address = getUserIP();

$sql = "SELECT Address FROM Tab_BannedIP WHERE Address='" . $address . "'";
$result = $conn->query($sql) or die ('Something has gone wrong, try again later');

if($result->num_rows > 0){
	header("location:login.php?e=2");
	exit();
}

$sql = "SELECT T, ForcePass FROM Tab_Reg WHERE UserName = '" . $user . "' AND Verified='1'";
$result = $conn->query($sql);

if(!$result){
	die ('Something has gone wrong, try again later');
}

if($result->num_rows > 0){
	$row = $result->fetch_assoc();
	if(password_verify($pass, $row['T'])){
		$pass = $row['T'];
                if ((int) $row["ForcePass"] !== 1) {
                        header("location:login.php?e=3");
                        exit(0);
                }
	}
}else{
    header("location:login.php?e=1");
    exit(0);
}

$sql = "SELECT id FROM Tab_Reg WHERE UserName = '" .$user."' AND T = '".$pass."' AND Verified='1'";
$result = $conn->query($sql);

if(!$result){
	die ('Something has gone wrong, try again later');
}

if($result->num_rows > 0){
	$row = $result->fetch_assoc();
	session_start();
	$_SESSION["id"] = $conn->real_escape_string($row['id']);
	header("location:index.php");
	$conn->query("UPDATE Tab_Reg SET Address='" . $address . "' WHERE UserName='" . $user . "'");
}else{
	header("location:login.php?e=1");
}

?>
