<?php
include 'db.php';

$pub = $conn->real_escape_string($_REQUEST['pub']);

if (strlen($pub) !== 128) {
	header("location:index.php");
	exit(0);
}

$pass = $conn->real_escape_string($_REQUEST['pass']);

$sql = "SELECT T, ResetExpire FROM Tab_Reg WHERE ResetToken='" . $pub . "'";
$result = $conn->query($sql);

if ($result->num_rows <= 0) {
    header("location:newpassword.php?e=1&?k=" . $pub);
	exit(0);
}

$row = $result->fetch_assoc();

if ($row["ResetExpire"] <= time()) {
    header("location:newpassword.php?e=2&k=" . $pub);
    exit(0);
}

if (password_verify($pass, $row["T"])) {
    header("location:newpassword.php?e=3&k=" . $pub);
    exit(0);
}

$sql = "UPDATE Tab_Reg SET T='" . password_hash($pass, PASSWORD_DEFAULT) . "', ResetToken=NULL, ResetExpire=NULL, ForcePass=1 WHERE ResetToken='" . $pub . "'";
$conn->query($sql);

header("location:newpassword.php?t=1");
exit();
?>
