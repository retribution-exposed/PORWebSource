<?php
include 'timeUtils.php';
include 'sections/header.php';

$email = "";

$sql = "SELECT * FROM Tab_Reg WHERE id = '" . $conn->real_escape_string($_SESSION["id"]) . "'";
$result = $conn->query($sql);

if (!$result) {
    die('Something has gone wrong, try again later');
}

if ($result->num_rows <= 0) {
    die('Log in first.');
    exit(0);
}

$row = $result->fetch_assoc();
$email = $conn->real_escape_string($row["Email"]);
$bannedUntil = $row["BannedUntil"];
$bannedMessage = "";

if (is_numeric($bannedUntil)) {
    $bannedUntil = (int) $bannedUntil;

    if ($bannedUntil === 1) {
        $bannedMessage = "forever";
    } else if ($bannedUntil > time()) {
        $bannedMessage = getHumanTime($bannedUntil - time());
    }
}
?>
<div class="page_center">
    <h1>Edit Account Information</h1>

    <?php
if ($_GET["e"] == 1) {
?>
    	<br>
			<h2><font color="red">There is already an account with that Username</font></h2>
			<br>
    <?php
} else if ($_GET["e"] == 2) {
?>
    		<br>
			<h2><font color="red">There is already an account with that email</font></h2>
			<br>
	<?php
} else {
?>

    <?php
    if ($bannedMessage !== "") {
?>
        <h2>You are banned for <?php
        echo $bannedMessage;
?>.</h2>
    <?php
    }
?>
		    <h2>To update account information, change below.</h2>
		    <h2>To update password, enter below. Otherwise leave fields blank</h2>
		    <br>

    <?php
}
?>
    <form id="Form1" action="_a.php" method="post">
    <div style="width:535px;" align="right">
		    <label>Username: </label><input type="text" value="<?php
echo $row["UserName"];
?>" readonly><br><br>
				<label>Account Type: </label><input type="text" value="<?php
switch ($row["Access"]) {
    case 100:
        echo "Regular";
        break;
    case 110:
        echo "Supporter";
        break;
    case 120:
        echo "Pirate Captain";
        break;
    case 130:
        echo "Pirate Lord";
        break;
    case 140:
        echo "Pirate King";
        break;
    case 150:
        echo "Pirate God";
        break;
    case 300:
        echo "Media Team";
        break;
    case 700:
        echo "Game Master";
        break;
    case 800:
        echo "Game Master";
        break;
    case 900:
        echo "Game Master";
        break;
    case 1000:
        echo "System Admin";
        break;
}
?>" style="width:130px;" readonly><div class="button-chk" style="height: 40px;  width: 100px;" id="button-checkout"><a href="rewards-store/"><span style="font-size: 16px;">Upgrade</span></a></div><br><br>
				<label>Existing Email: </label><input type="text" value="<?php
echo $email;
?>" readonly><br><br>
      	<label>New Email: </label><input type="text" id="email" name="email" value="<?php
echo $email;
?>"><br><br>
				<label>Confirm New Email: </label><input type="text" id="emailconf" name="emailconf"><br><br>
      	<label>New Password: </label><input type="password" id="pass" name="pass"><br>
      	<i>Must be at least 6 characters</i><br>
      	<div style="margin:5px;"></div>
      	<label>Confirm New Password: </label><input type="password" id="conf" name="conf"><br>
      	<i>Case sensitive</i>
      	<br><br>
      	<div class="button">
        	<a href="#" onClick="Validate();return false;"><span>Update</span></a>
        </div>
    </div>
    </form>
</div>
<script>
function isEmail(email) {
  var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  return regex.test(email);
}

function Validate(){
	var flgIsValid = false;
	var e = "";

	if($('#user').val() == ""){
		e = e + "\nPlease Enter Username";
	}
	if($('#email').val() == ""){
		e = e + "\nPlease Enter Email";
	}
	if(isEmail($('#email').val()) == false){
		e = e + "\nPlease Enter a Valid Email";
	}
	if($('#emailconf').val() == ""){
		e = e + "\nPlease Enter Confirmation Email";
	}
	if($('#email').val() != $('#emailconf').val()){
		e = e + "\nEmails do not match"
	}
	if($('#pass').val() != ""){
		if($('#pass').val().length < 6){
			e = e + "\nPassword must be at least 6 characters";
		}
		if($('#conf').val() == ""){
			e = e + "\nPlease Enter Confirmation Password";
		}
		if($('#pass').val() != $('#conf').val()){
			e = e + "\nPasswords do not match"
		}
	}

	if(e == ""){
		flgIsValid = true;
	}

	if(flgIsValid){
		$('#Form1').submit();
	}else{
		alert("There are the following issues:" + e);
	}
}
</script><br><br><br><br><br><br>
<?php
include 'sections/footer.php';
?>
