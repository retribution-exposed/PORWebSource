<?php $options['ship'] = true; ?>
<?php include 'sections/header.php'; ?>
<style>
.conten_footer {
    position: absolute;
    background-repeat: repeat-y;
    height: 335px;
    object-fit: cover;
    z-index: 11;
    margin-left: -26px;
    width:860px;
    bottom: 0;
}
.content_page {
    position: relative;
    background-repeat: repeat-y;
    width: 800px;
    line-height: 30px;
    object-fit: fill;
    margin: 0 auto 0 auto;
    z-index: 0;
    margin-top: -100px;
    padding: 100px 10px 0 50px;
    text-align: left;
}
p {
    font-weight: bold;
    line-height: 23px !important;
    font-family: 'sans-serif' !important;
    text-shadow: #ffffff 0px 0px 0px;
}
img.stafficon {
    transform: translateY(36%);
    width: 2.5vw;
}
</style>
<div class="page_center">
<div style="position: relative; margin-top: -50px;">
</div>
<?php
        if($_GET["m"] == 1){
    ?>
        <br>
            <h2><font color="red">Comment submitted for approval</font></h2>
    <?php
        }else if($_GET["e"] == 2){
    ?>
            <br>
            <h2><font color="red">There is already an account with that email</font></h2>
    <?php
        }else if($_GET["e"] == 3){
    ?>
            <br>
            <h2><font color="red">Please enter captcha</font></h2>
    <?php
        }
    ?>
<?php
$id = $conn->real_escape_string($_REQUEST['id']);
$sql = "SELECT * FROM Tab_LauncherNews WHERE id=".$id;
$result = $conn->query($sql) or die('Something has gone wrong, try again later');

if($result->num_rows > 0){
    $row = $result->fetch_assoc();
    echo '<h1 class="skull_large" id="'.$row['id'].'" name="'.$row['id'].'">'.$row['Title'].'</h1>';
    echo '<div class="box400 center"><div class="news-date">'.$row['Description'].'<br style="margin: 0;padding: 0" />'.$row['Date'];
    echo '</div></div>';
    echo '<br style="margin: 0;padding: 0"/>';
    echo $row['Message'];
    echo '<br style="margin-bottom: 0px;padding: 0"/><span style="font-family: \'sans-serif\'; font-weight: bold; font-size: 15px">Fair Winds</span><br style="margin-bottom: 25px;padding: 0" />~<span style="font-family: \'sans-serif\'; font-weight: bold; font-size: 15px"> <i>The Crew @ Pirates Online Retribution</i></span><br style="margin-bottom: 8px;padding: 0"/>';
    echo '<div class="sep"></div>';
    echo '<h1>Comments</h1><br>';

    $sql = "SELECT Tab_NewsComments.*, Tab_Reg.UserName, Tab_Reg.Access FROM Tab_NewsComments LEFT JOIN Tab_Reg ON Tab_NewsComments.UserName=Tab_Reg.UserName WHERE Status=2 AND PostId=".$id." ORDER BY id";
    $result2 = $conn->query($sql) or die('Something has gone wrong, try again later');
    $stafficon = "https://piratesforums.com/screenshots/8cmh9ne.png";

    if($result2->num_rows > 0){
      $n = 0;
echo '<div class="row2">';
      while($row2 = $result2->fetch_assoc()) {
          echo '<div class="col-3">';
            echo '<label><u>'.$row2['UserName'] . "</u></label><br>";
            echo $row2['Comment'] . "<br>";
            echo '</div>';
        }
    }else{
        echo '</div><br><br>No comments<br><br>';
    }

    session_start();
    $sql = "SELECT UserName FROM Tab_Reg WHERE id = '" . $conn->real_escape_string($_SESSION["id"]) . "'";
    $result3 = $conn->query($sql);

    if(!$result3){
        die ('Something has gone wrong, try again later');
    }

    if($result3->num_rows > 0){
        $row3 = $result3->fetch_assoc();
    ?>
        <br>
        <form id="Form1" action="_c.php" method="post">
        <div style="width:535px;" align="right">
        <?php
            echo '<input type="hidden" id="user" name="user" value="';
            echo $row3['UserName'];
            echo '"/>';
            echo '<input type="hidden" id="id" name="id" value="';
            echo $id;
            echo '"/>';
        ?>
            <label style="float:left;margin-left:235px;">Comment: </label><br><textarea style="resize:vertical;width:300px;height:50px;font-size:18px;" id="comment" name="comment"></textarea><br><br>
            <div class="g-recaptcha" data-sitekey="6Ld86ikTAAAAAJ4wlfZ3R2BYRp9WGRd-q44-xDpA"></div><br>
            <div class="button">
                <a href="#" onClick="Validate();return false;"><span>Post</span></a>
            </div></div>
        </div>
      </form>
    <?php
    }else{
        echo "'</div>'<br><br><br>You must log in to comment";
    }

    echo '<br><br><br><br><br><br><br><br>';
}
?>

</div>
<script>
function Validate(){
    var flgIsValid = false;
    var e = "";

    if($('#comment').val() == ""){
        e = e + "\nPlease Enter Comment";
    }

    if(e == ""){
        flgIsValid = true;
    }

    if(flgIsValid){
        $('#Form1').submit();
    }else{
        alert("There are the following issues:" + e);
    }
}
</script>
<?php include 'sections/footer.php';?>
