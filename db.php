<?php
$conf = parse_ini_file("/home/game/site.ini");

global $secrettoken, $emailUser, $emailPass;
$servername = "localhost";
$username = "root";
$password = $conf["sqlPassword"];
$secrettoken = $conf["secretToken"];
$emailUser = $conf["emailUser"];
$emailPass = $conf["emailPass"];
$db = "trx";

global $conn;

$conn = new mysqli($servername, $username, $password, $db);

if ($conn->connect_error) {
    die("Something is wrong, please try again later");
}
?>
