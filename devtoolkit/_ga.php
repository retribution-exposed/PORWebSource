<?php include '../db.php';
header('Content-Type: application/json');

if (!isset($_POST['name']) || !isset($_POST['secrettoken'])) {
	echo json_encode(array('status' => 'error', 'code' => '0', 'message' => 'What do you mean?'));
	exit(0);
}

if ($secrettoken !== $_POST['secrettoken'] || $_SERVER['HTTP_USER_AGENT'] !== 'POR-ClientServicesManagerUD') {
	echo json_encode(array('status' => 'error', 'code' => '1', 'message' => 'Who are you?'));
	exit(0);
}

$name = $conn->real_escape_string($_POST['name']);
$extraInfo = $_POST['extraInfo'];

$sql = "SELECT Status FROM Tab_GuildNames WHERE Name='" . $name . "'";
$result = $conn->query($sql);

if($result->num_rows > 0){
	$row = $result->fetch_assoc();
        $status = $row["Status"];

        if ($extraInfo === "New") {
                if ($status !== 0) {
                        echo json_encode(array('status' => 'success', 'code' => '3', 'nameStatus' => '3', 'message' => 'Name already exists.'));
                        exit(0);
                }
        } else if ($extraInfo === "Remove") {
                $sql = "DELETE FROM Tab_GuildNames WHERE Name='" . $name . "'";
                $conn->query($sql);
                echo json_encode(array('status' => 'success', 'code' => '4', 'nameStatus' => '0', 'message' => 'Removed guild name.'));
                exit(0);
        }

	echo json_encode(array('status' => 'success', 'code' => '2', 'nameStatus' => $row["Status"], 'message' => 'Pulled name status.'));
	exit(0);
}

if ($extraInfo !== "Remove") {
        $sql = "INSERT INTO Tab_GuildNames (Name, Status) VALUES ('" . $name . "', 0)";
        $conn->query($sql);
        echo json_encode(array('status' => 'success', 'code' => '2', 'nameStatus' => '0', 'message' => 'Sent name for review.'));
}
?>
