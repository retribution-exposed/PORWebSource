<?php
include '../db.php';

$user = $conn->real_escape_string($_REQUEST['user']);
$pass = $conn->real_escape_string($_REQUEST['pass']);

$sql = "SELECT T FROM Tab_Reg WHERE UserName = '" . $user . "' AND Verified='1' AND Access > 899";
$result = $conn->query($sql);

if(!$result){
	die ('Something has gone wrong, try again later');
}

if($result->num_rows > 0){
	$row = $result->fetch_assoc();
	if(password_verify($pass, $row['T'])){
		$pass = $row['T'];
	}
}else{
	header("location:login.php?e=1");
	exit();
}

$sql = "SELECT id FROM Tab_Reg WHERE UserName = '" .$user."' AND T = '".$pass."' AND Verified='1' AND Access > 899";
$result = $conn->query($sql);

if(!$result){
	die ('Something has gone wrong, try again later');
}

if($result->num_rows > 0){
	$row = $result->fetch_assoc();
	session_start();
	$_SESSION["id"] = $conn->real_escape_string($row['id']);
	header("location:main.php");
	exit();
}else{
	header("location:login.php?e=1");
	exit();
}
?>
