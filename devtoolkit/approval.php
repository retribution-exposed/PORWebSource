<?php
include '../sections/head.php';
include '../db.php';

session_start();
$sql = "CALL CheckAccessLevel('" . $conn->real_escape_string($_SESSION["id"]) . "', '900')";
$result = $conn->query($sql);
$conn->next_result();

if($result->num_rows > 0){
    $row = $result->fetch_assoc();
?>
<a href="main.php">Back</a><br><br><Br>
<form id="Form1" action="_nu.php" method="post">
<input type='hidden' id='operation' name='operation' value='jes' />
<input type='hidden' id='selected' name='selected' value='||' />
<div style="overflow:scroll; height: 400px; width:250px; list-style-type: none;" align="left">
    <?php
        $sql = "CALL GetPendingNames";
        $result = $conn->query($sql);
        $conn->next_result();

        if(!$result){
            die ('Something has gone wrong, try again later');
        }

        if($result->num_rows > 0){
            echo "<ul>";
            while($row = $result->fetch_assoc()) {
                echo "<li><input type='checkbox' id='".$row["id"]."' name='".$row["id"]."' />".$row["Name"]."</li>";
            }
            echo "</ul>"; 
        }else{
    ?>
        No names awaiting approval
    <?php
        }?>
</div>
<div>
    <button onClick="Accept();">Accept</button>
    &nbsp;&nbsp;
    <button onClick="Decline();">Decline</button>
</div>
</form>
<img src="http://image.prntscr.com/image/3f74ac93fd2a436d95e6a10a6e06aa57.png" />
<?php
        if($_GET["m"] == 1){
    ?>
            <h2><font color="red">Names Approved</font></h2>
    <?php
        }else if($_GET["m"] == 2){
            echo '<h2><font color="red">Names Declined</font></h2>';
        }
    ?>
<script>
    function Accept(){
        var selected="";

        $("input:checkbox").each(function(){
            var $this = $(this);

            if($this.is(":checked")){
                selected += $this.attr("id") + "|";
            }
        });

        $("#operation").val("jes");
        $("#selected").val(selected.slice(0, -1));

        $("Form1").submit();
    }

    function Decline(){
        var selected="";

        $("input:checkbox").each(function(){
            var $this = $(this);

            if($this.is(":checked")){
                selected += $this.attr("id") + "|";
            }
        });

        $("#operation").val("ne");
        $("#selected").val(selected.slice(0, -1));

        $("Form1").submit();
    }

    var time = new Date().getTime();
    $(document.body).bind("mousemove keypress", function(e) {
        time = new Date().getTime();
    });

    function refresh() {
        if(new Date().getTime() - time >= 30000) 
            document.location = "approval.php";
        else 
            setTimeout(refresh, 5000);
    }

     setTimeout(refresh, 5000);
</script>
<?php
}else{
	header("location:login.php");
	exit();
}