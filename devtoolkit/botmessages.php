<?php 
include '../db.php'; 

session_start();
$sql = "CALL CheckAccessLevel('" . $conn->real_escape_string($_SESSION["id"]) . "', '900')";
$result = $conn->query($sql);
$conn->next_result();

if($result->num_rows == 0){
    header("location:login.php");
    exit();
}

if(!empty($_POST['id'])) {
    $sql = "CALL GetDiscordBotMessagesByID ('".$conn->real_escape_string($_POST['id'])."')";
	$result = $conn->query($sql);
	$row = $result->fetch_assoc();
    $conn->next_result();
    echo json_encode(array("message"=>$row['Message']));
    exit;
}

include '../sections/head.php';
?>
    <a href="main.php">Back</a><br><br><Br>
    <form id="Form1" action="_bm.php" method="post">
        <input type="hidden" id="title" name="title" value="" />
    	<select id="selections" name="selections">
		  <option value="new">New Message</option>
		  <?php
		  	$sql = "CALL GetDiscordBotMessages";
			$result = $conn->query($sql);
            $conn->next_result();

			if($result->num_rows > 0){
                $i = 1;
	            while($row = $result->fetch_assoc()) {
	                echo "<option value='".$row["id"]."'>".$i."</option>";
                    $i = $i + 1;
	            }
        	}	
		  ?>
		</select>
		<div style="margin:5px;"></div>
       	<label>Message: </label><textarea id="message" name="message" style="width:250px;height:50px;"></textarea><br><br>
      	<input type="button" onClick="Validate(1);" value="Create/Update" />
      	<input type="button" onClick="Validate(2);" value="Delete" />
    </form>
    <?php
        if($_GET["m"] == 1){
    ?>
            <h2><font color="red">Message Added</font></h2>
    <?php
        }else if($_GET["m"] == 2){
            echo '<h2><font color="red">Message Deleted</font></h2>';
        }else if($_GET["m"] == 3){
            echo '<h2><font color="red">Message Updated</font></h2>';
        }
    ?>
<script>
$('#selections').change(function(){ 
    var value = $(this).val();

    if(value == "new"){
    	$("#message").val("");
    }else{
    	$.ajax({
            url: 'botmessages.php',
            type: 'post',
            
            data: { id: value },
            success: function(response) {
                var Vals = JSON.parse(response);
                $("#message").val(Vals.message);
            }
        });
    }
});

function Validate(type){
	var flgIsValid = false;
	var e = "";

	if(type == 1){
		if($('#message').val() == ""){
			e = e + "\nPlease Enter Message";
		}

		if(e == ""){
			flgIsValid = true;
		}

		if(flgIsValid){
			$('#Form1').submit();
		}else{
			alert("There are the following issues:" + e);
		}
	}else{
		$("#title").val("forigu");
		$('#Form1').submit();
	}
}
</script>