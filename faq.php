<?php include 'sections/header.php'; ?>

<div class="page_center">
	<h1>Frequently Asked Questions</h1>
	<div class="sep"></div><br>
  <img src="https://piratesforums.com/screenshots/8xgvljo.png"/>
	<br><br>
	<h1>Table of Contents</h1>

	<h2>General</h2>
	<ul class="FAQ" style=" list-style-type: none; padding: 1px;">
		<li><a href="#1">What is Pirates Online Retribution?</a></li>
		<li><a href="#2">How can I begin to play?</a></li>
		<li><a href="#3">Will there ever be updates and new content?</a></li>
		<li><a href="#4">Can I play as my old pirate from POTCO?</a></li>
		<li><a href="#5">Are there rules in the game?</a></li>
		<li><a href="#6">How much does it cost to play?</a></li>
		<li><a href="#10">Do I need a Beta key in order to play?</a></li>
		<li><a href="#11">What is the Rewards store?</a></li>
  </ul>
	<h2>Technical</h2>
	<ul class="FAQ" style=" list-style-type: none; padding: 1px;">
		<li><a href="#7">I'm having trouble with the game, what's wrong?</a></li>
		<li><a href="#8">I found a bug, how can I report it?</a></li>
		<li><a href="#9">What is the Forums?</a></li>
	</ul>

	<br><br>
	<h1>General</h1>

    <h2 id="1">What is Pirates Online Retribution?</h2>
    <p>Pirates Online Retribution is a fan-made revival of Disney's closed MMORPG, Pirates of the Caribbean Online. For more information on who we are, click <a href="about.php">here</a> to visit our About Us page.</p>

    <h2 id="2">How can I begin to play?</h2>
    <p>Pirates Online Retribution is publicly accessible to all, and is 100% free and always will be! To play, simply register an account and download our game.</p>

    <h2 id="3">Will there ever be updates and new content?</h2>
    <p>Yes! We are constantly making updates and releasing new content to enhance your gaming experience.</p>

    <h2 id="4">Can I play as my old pirate from POTCO?</h2>
    <p>Unfortunately, we cannot transfer pirates over from Disney's game. Sorry!</p>

    <h2 id="5">Are there rules in the game?</h2>
    <p>Yes, there are rules in the game. These rules are to keep you safe online and to ensure that you have the most enjoyable and family friendly experience possible. All players must agree to follow our <a href="terms.php">Terms of Service</a> in order to play the game, and violations of the rules may result in serious action being taken against your account.</p>

    <h2 id="6">How much does it cost to play?</h2>
    <p>Pirates Online Retribution is 100% free and always will be!</p>

		<h2 id="10">Do I need a Beta key in order to play?</h2>
		<p>No. Pirates Online Retribution is fully public, and always will be. We will never required "keys" in order for you to access our game. You can play whenever you like, for as long as you like.</p>

		<h2 id="11">What is the Rewards store?</h2>
		<p>The Rewards store was created for the purpose of raising donations to help fund POR's servers, advertisements, development bounties, and other expenses. Although POR is 100% free, and always will be, we immensely appreciate those who go out of their way to help finance the continuation of our project.</p>

    <br><br>
    <h1>Technical</h1>

    <h2 id="7">I'm having trouble with the game, what's wrong?</h2>
    <p>If you encounter any issues during gameplay, be sure to contact our support team on the Pirates Forums, or Facebook, or email support@piratesonline.us</p>

    <h2 id="8">I found a bug, how can I report it?</h2>
    <p>To report a bug to us, please use the feedback tool in-game or alternatively, you can email support@piratesonline.us</p>

    <h2 id="9">What is the Forums?</h2>
    <p>The Forums is the hub of player interactivity off the game. It also serves as somewhat of an "archive" for all POR related game, and community content. You can visit the Forums by clicking <a href="https://piratesforums.com">here</a></p>

    <br><br>
    <h1>Contact us</h1>
    <br>

    <p>Have more questions? You can email us at: <a href="mailto:support@piratesonline.us">support@piratesonline.us</a><br>
    or join the official Pirates Forums at: <a href="http://piratesforums.com">https://piratesforums.com</a></p>
    <br>
    <p><a href="#">Back to Top</a></p>
<br><br><br><br>

</div>
<?php include 'sections/footer.php';?>
