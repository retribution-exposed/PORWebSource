<?php include 'sections/header.php'; ?>

<div class="page_center">
    <h1>Login</h1>

    <?php
    	if($_GET["e"] == 1){
    ?>
    	<br>
			<h2><font color="red">Incorrect Username/Password</font></h2>
			<br>
    <?php
    	} else if($_GET["e"] == 2){
    ?>
    	<br>
            <h2><font color="red">This IP has been banned. If you believe this was a mistake, please contact us at support@piratesonline.us</font></h2>
            <br>
    <?php
    	} else if($_GET["e"] == 3){
    ?>
    	<br>
            <h2><font color="red">Avast! For security purposes, all players are required to update their passwords prior to logging in again. Please <b><u><a href="password.php">reset your password</a></b></u>.</font></h2>
            <br>
	<?php
		}else{
			echo '<br><br>';
		}
	?>

    <form id="Form1" action="_l.php" method="post">
    <div style="width:535px;" align="right">
       	<label>UserName: </label><input type="text" id="user" name="user"><br>
       	<div style="margin:5px;"></div>
    	<label>Password: </label><input type="password" id="pass" name="pass"><br>
    	<a href="password.php">Forgot Password?</a>
      	<br><br>
      	<div class="button">
        	<a href="#" onClick="Validate();return false;"><span>Login</span></a>
        </div>
    </div>
    </form>
</div>
<script>

function Validate(){
	var flgIsValid = false;
	var e = "";

	if($('#user').val() == ""){
		e = e + "\nPlease Enter Username";
	}

	if($('#pass').val() == ""){
		e = e + "\nPlease Enter Password"
	}

	if(e == ""){
		flgIsValid = true;
	}

	if(flgIsValid){
		$('#Form1').submit();
	}else{
		alert("There are the following issues:" + e);
	}
}
</script>
<?php include 'sections/footer.php';?>
