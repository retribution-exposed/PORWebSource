<?php include 'sections/header.php'; ?>
<div class="page_center">
    <h1>Reset Password</h1>

    <?php
    	if($_GET["e"] == 1){
    ?>
    	<br>
			<h2><font color="red">Not a valid token</font></h2>
			<br>
	<?php
		}else if ($_GET["e"] == 2) {
    ?>
        <br>
			<h2><font color="red">Token has expired</font></h2>
			<br>
    <?php
		}else if ($_GET["e"] == 3) {
    ?>
        <br>
			<h2><font color="red">Please choose another password</font></h2>
			<br>
    <?php
        } else {
			echo '<br><br>';
		}

		if($_GET["t"] == 1){
			echo "<br><h2><font color='red'>Password reset</font></h2><br>";
		}
	?>

    <form id="Form1" action="_p.php" method="post">
    <input type="hidden" id="pub" name="pub" value="<?php echo $_GET['k'];?>"/>
    <div style="width:535px;" align="right">
       	<label>Password: </label><input type="password" id="pass" name="pass"><br>
      	<i>Must be at least 6 characters</i><br>
       	<div style="margin:5px;"></div>
    	<label>Confirm Password: </label><input type="password" id="conf" name="conf"><br>
      	<br><br>
      	<div class="button">
        	<a href="#" onClick="Validate();return false;"><span>Update</span></a>
        </div>
    </div>
    </form>
</div>
<script>
function Validate(){
	var flgIsValid = false;
	var e = "";

	if($('#pass').val() == ""){
		e = e + "\nPlease Enter Password";
	}

	if($('#pass').val() != $('#conf').val()){
		e = e + "\nPassword's do not match"
	}
	if($('#pass').val().length < 6){
		e = e + "\nPassword must be at least 6 characters";
	}

	if(e == ""){
		flgIsValid = true;
	}

	if(flgIsValid){
		$('#Form1').submit();
	}else{
		alert("There are the following issues:" + e);
	}
}
</script>
<?php include 'sections/footer.php';?>