<?php
if (!isset($_POST['receiver_id']) || !isset($_POST['payer_email']) || !isset($_POST['payment_status']) || !isset($_POST['item_name'])) {
    exit(0);
}

if ($_POST['payment_status'] !== 'Completed') {
    exit(0);
}

$receiver = urldecode($_POST['receiver_id']);

if ($receiver !== '4J77EAD62JCE2') {
    exit(0);
}

require('lib/PaypalIPN.php');
use PaypalIPN;

$ipn = new PaypalIPN();

if (!$ipn->verifyIPN()) {
    exit(0);
}

include 'db.php';

$payer = $conn->real_escape_string(urldecode($_POST['payer_email']));

$sql = "SELECT UserName FROM Tab_Reg WHERE Email='" . $payer . "' AND Access < 200";
$result = $conn->query($sql);

if ($result->num_rows <= 0) {
    exit(0);
}

$row = $result->fetch_assoc();
$username = $row['UserName'];

$item = urldecode($_POST['item_name']);
$expires = time() + 2592000;

if ($item === "Supporter") {
    $access = 110;
} else if ($item === "Pirate Captain") {
    $access = 120;
} else if ($item === "Pirate Lord") {
    $access = 130;
} else if ($item === "Pirate King") {
    $access = 140;
} else if ($item === "Pirate God") {
    $access = 150;
    $expires = 'NULL';
} else {
    exit(0);
}

$conn->query("UPDATE Tab_Reg SET Access=" . $access . ", RankExpire=" . $expires . " WHERE UserName='" . $username . "'");
header("HTTP/1.1 200 OK");
?>

