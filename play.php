<?php include 'sections/header.php'; ?>

<!--http://tinyurl.com/ztz4bwr-->
<div class="page_center">
    <h1>Sail the Seas!</h1>
    <br>
    <img src="https://piratesonline.us/images/compass.png" style="width: 15%;"/>
    <img src="https://piratesforums.com/screenshots/zyp0crs.png" style="width: 15%; height: 225px; float: right; padding-right: 25px;"/>
    <img src="https://piratesforums.com/screenshots/95wpxtl.png" style="width: 15%; float: left; height: 225px; padding-left: 25px;"/>
    <h2>System Requirements</h2>
    <table class="PlayGame" style="table-layout:fixed;width:100%;margin-left:-5px;">
        <thead>
        <tr>
            <th>Hardware</th>
            <th>Minimum</th>
            <th>Recommended</th>
        </tr>
        </thead>
        <tbody>
          <tr>
            <td>Operating System</td>
            <td>Windows 7 / macOS Yosemite</td>
            <td>Windows 8 or newer / macOS El Capitan (10.11) or newer</td>
        <tr>
            <td>Hard disk space</td>
            <td>700 MB</td>
            <td>2GB or more</td>
        </tr>
        <tr>
            <td>RAM</td>
            <td>2GB</td>
            <td>4GB or more</td>
        </tr>
        <tr>
            <td>CPU</td>
            <td>Dual-core CPU</td>
            <td>Quad-core CPU or better</td>
        </tr>
        <tr>
            <td>Graphics Card</td>
            <td>OpenGL-compatible card with at least 256 MB of graphics memory</td>
            <td>OpenGL-compatible card with at least 1 GB of graphics memory</td>
        </tr>
        <tr>
            <td>Internet Connection</td>
            <td>Any internet connection</td>
            <td>Broadband Connection or greater</td>
        </tbody>
    </table>
    <br>
    <?php
    session_start();
    $sql = "SELECT UserName FROM Tab_Reg WHERE id = '" . $conn->real_escape_string($_SESSION["id"]) . "'";
    $result = $conn->query($sql);
    if(!$result){
        die ('Something has gone wrong, try again later');
    }
    if($result->num_rows > 0){
        $row = $result->fetch_assoc();
    ?>
    Pirates Online Retribution may work on macOS 10.9 or below,<br>
    but due to their age they are not officially supported by our launcher.<br><br>
        <ul class="nav" style="top: 0px;">
            <li>
                <div class="button">
                    <a href="http://tinyurl.com/ztz4bwr" target="_blank"> <span>Windows</span></a>
                </div>
            </li>
            <li>
                <div class="button">
                    <a href="http://tinyurl.com/zsw3xes" target="_blank"> <span>Mac</span></a>
                </div>
            </li>
          </ul>
    <br>
    <?php
    }else{
    ?>
    <br><br>
        Avast! Ye must register an account, or login in order to download the game!
    <?php
    }
    ?>
</div><br><br><br>
<?php include 'sections/footer.php';?>
