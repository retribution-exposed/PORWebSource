<div class="conten_footer_about">&nbsp;
				<?php
				if(isset($options['ship'])):?>
					<div style="position: absolute; width: 369px; bottom:0">
						<img src="images/footer_ship.png" />
					</div>
				<?php endif;?>
			</div> <!-- page content footer image -->
		</div> <!-- page_content -->
	</div> <!-- content -->
</div> <!-- header's id=container -->
<div class="footer_wrapper">
	<div class="footer site_center">
		<div class="partners">
	<a href="https://www.facebook.com/potcomc/?fref=ts"><img src="images/potcomc.png"></a> <a href="https://www.facebook.com/PotcoMemories/"><img src="images/hollyrogers.png"></a> <a href="https://www.youtube.com/user/TMPCast"><img src="http://i.imgur.com/RCaqj9s.png" style="padding-bottom:25px" width="156"></a>
  </div>
		<div class="social" style="padding-top:10px;">
<img src="images/gplus.png"> <a href="https://twitter.com/potcretribution"><img src="images/twitter.png"></a>
	<a href="https://www.youtube.com/user/POTCOGuides"><img src="images/youtube.png"></a> <a href="https://www.facebook.com/POTCORetribution"><img src="images/fb.png"></a>
</div>
		<div class="foot_msg">
			<p>
				<span style="font-size:12px;font-style:italic;color:rgb(255, 255, 255); font-family:times new roman,times,serif">"Restoring lost hope to a very special community..." </span>
				<br style="margin:0"/>
				<span style="font-size:12px;font-style:italic;color:rgb(255, 255, 255); font-family:times new roman,times,serif"> - Pearson Wright </span>


			</p>
		</div>
		<div class="footer_links" style="padding-right: 50px;">

			<ul>
				<li>
					<a href="/">Home</a>
				</li>
				<li>
					<a href="http://piratesforums.com" target="_blank">Forums</a>
				</li>
				<li>
					<a href="faq.php">FAQ</a>
				</li>
				<li>
					<a href="thecode.php">The Code</a>
				</li>
				<li>
					<a href="about.php">About</a>
				</li>
				<li>
					<a href="login.php" target="_blank">Login</a>
				</li>
				<li>
					<a href="contact.php">Contact</a>
				</li>
				<li>
					<a href="news.php">News</a>
				</li>
			</ul>

		</div>

	</div>
</div>
</body>
</html>
