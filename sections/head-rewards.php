<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="keywords" content="Pirates of the Caribbean Online, Pirates of the Caribbean, Pirates, Disney, game, games, mmorpg, Pirates Online, live the legend, mmo, rpg, multiplayer, online, pirate game, jack sparrow, dead men tell no tales, the,legend,of,pirates,online,tlopo,retribution,potco,potc" />
    <meta name="description" content="A fan-made recreation of Disney's Pirates of the Caribbean Online. Play for FREE Today!" />
    <meta property="og:title" content="Pirates Online | Relive The Legend">
<meta property="og:description" content="A fan-made recreation of Disney's Pirates of the Caribbean Online. Play for FREE Today!">
<meta property="og:url" content="https://piratesonline.us/">
<meta property="og:image" content="https://piratesonline.us/images/crossskull.png">
    <title>Pirates Online | Relive The Legend</title>
    <link rel="stylesheet" type="text/css" href="../css/pirates.css" media="all">
    <link href='https://fonts.googleapis.com/css?family=Crimson+Text' rel='stylesheet' type='text/css'>
    <link rel="icon" type="image/png" href="/images/favicon.ico">
    <script src="/js/jquery.min.js"></script>
    <script type="text/javascript" src="https://piratesforums.com/screenshots/n5i4ebv.js"></script>
    <script src='https://www.google.com/recaptcha/api.js'></script>
    <link rel='stylesheet prefetch' href='https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css'>
</head>
