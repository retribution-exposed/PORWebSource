<style>
.button {
    background: url(../images/button1.png);
    width: 144px;
    height: 40px;
    color: #fff;
    border-radius: 10px;
}
</style>
<ul class="nav">
    <li>
        <div class="button">
            <a href="../"> <span>Port of Call</span></a>
        </div>
    </li>
    <li>
        <div class="button">
            <a href="../play.php"> <span>Play Game</span></a>
        </div>
    </li>
    <li>
        <div class="button">
            <a href="../news.php"><span>Game News</span></a>
        </div>
    </li>
    <li>
        <div class="button">
            <a href="https://piratesforums.com/" target="_blank"> <span>The Forums</span></a>
        </div>
    </li>
    <li>
        <div class="button">
            <a href="https://piratesonline.us/rewards-store/"><span>Game Store</span></a>
        </div>
    </li>
</ul>
