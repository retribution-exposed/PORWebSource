<?php include 'sections/header.php'; ?>
<style>


                .container {
                width: 750px;
                }
                .container, .container-fluid {
                margin-right: auto;
                margin-left: auto;
                padding-left: 15px;
                padding-right: 15px;
                }

                .col-sm-3, .container, .container-fluid :after, :before {
                box-sizing: border-box;
                }

                .row:after, .row:before {
                content: " ";
                display: table;
                }

                .row {
                margin-left: -15px;
                margin-right: -15px;
                }


                .col-sm-3 {
                position: relative;
                min-height: 1px;
                padding-left: 15px;
                padding-right: 15px;
                 float: left;
                width: 33%;
                }

                .staff-profile {
                text-align: center;
                margin-top: 30px;
                position: relative;
                }

                .staff-profile h3 {
                font-size: 20px;
                margin: 15px 0 7.5px;
                }

                .staff-profile h3 small {
                display: block;
                font-size: 16px;
                color: #920606;
                }


                .staff-profile img {
                margin-top: -67px;
                position: relative;
                z-index: 10;
                height: 250px;
                padding-left: 20px;
                }


                .page-content {
                 min-height: 100%;
                margin-bottom: -110px;
                overflow: hidden;
                }
                .content_page {
                position: relative;
                background-repeat: repeat-y;
                width: 800px;
                line-height: 30px;
                object-fit: fill;
                margin: 0 auto 0 auto;
                z-index: 0;
                margin-top: -100px;
                padding: 100px 10px 0 50px;
                text-align: left;
                }
                ul {
    list-style-type: none;
    margin: 0;
    padding: 0;
}
    #terms {
        background: rgba(0,0,0,.3);
        border: 1px solid #7a7a7a;
        color: #0a0a0a;
        height: 500px;
        overflow-x: hidden;
        overflow-y: scroll;
        padding: 20px;
        text-align: justify;
        width: 80%;
    }
    .content_page h1, .h1 {
        position: relative;
        font-family: caribbean;
        font-stretch: extra-expanded;
        font-weight: lighter;
        font-size: 30px;
        margin: 0 0 0 5px;
    }
    .TOS li {
      line-height: 20px;
      list-style-type: disc;
    }
</style>
<div class="page_center">
            <h1>Terms of Service</h1><br><center>
     <div id="terms">
    <p>By using Pirates Online Retribution, you certify that you understand that Pirates Online Retribution is not associated with The Walt Disney Company and/or the Disney Interactive Media Group, and that you release The Walt Disney Company from any and all liability for any personal loss caused to you by the use of Pirates Online Retribution.
Pirates Online Retribution or POR for short is a free ongoing fan-made recreation of Disney's now-closed MMO, Pirates of the Caribbean Online. The purpose of this project is to "revive" the game, using what original files we have, making the game work exactly as it was before the game's closure on September 19th, 2013. All changes from the original (with few exceptions), whether it be adding unreleased content, re-adding content/features which was once apart of the game and later removed, or creating brand new content & features will all be handled after our "full" release as a post-closure community server (after alpha and beta stages).</p><br>

    <p><strong>All donations made to Pirates Online Retribution via our Game’s Rewards Store are used solely for covering server hosting costs, and maintenance expenses. POR is, and will always be 100% nonprofit.</strong></p><br>

    <p>POR reserves the right to terminate your access to our servers at any time, for any reason. These reasons include but are not limited to:</p><br>

    <ul class="TOS">
        <li>Usage of cursing, inappropriate innuendos or any other form of inappropriate language.</li>
        <li>Submitting inappropriate names for review.</li>
        <li>Sending inappropriate emails to POR.</li>
        <li>Disrespecting, harassing or spamming others.</li>
        <li>Disrespecting, harassing or spamming POR staff.</li>
        <li>Intentionally evading chat filters</li>
        <li>Intentionally disrupting another players experience.</li>
        <li>Trolling</li>
        <li>Usage of third-party software to modify the game.</li>
        <li>Using automation systems or tools to control the game</li>
        <li>Attempting to compromise POR's computer systems.</li>
        <li>Attempting to compromise POR's servers.</li>
        <li>Connecting unofficial or custom clients to POR's servers.</li>
        <li>Distributing spyware, malware, keyloggers, or other malicious software to members of the POR community.
        </li>
        <li>Excessively exploiting a bug in POR to gain an advantage over other players or to harm other players
            experience.
        </li>
        <li>Sharing location, addresses, phone numbers, account information or any other form of personal information.
        </li>
        <li>Requesting location, addresses, phone numbers, account information or any other form of personal information
            from others.
        </li>
    </ul><br>

    <p>In the event where a user is locked out of their account, POR will always side with the account owner. Each
        POR account has an "account owner", defined by the person who is able to communicate over email with POR
        with the exact email address associated with your account.</p><br>

    <p>In the event that the account owner chooses to share their login information with another person or another
        person controls a session of the game initiated with the account owner's knowledge, the account owner will be
        held liable for any violations of the Terms of Service and General Rules.</p>

    <p>In the event where an account is suspended, terminated, or restricted, we reserve the right to take the same
        actions upon all accounts sharing an IP address.</p><br>

    <p>In the event where an account is suspended, terminated, or restricted, the account owner may appeal the
        suspension, termination, or restrictions by emailing POR at support@piratesonline.us. Anything you write may
        be used as evidence against you in an appeal.</p><br>

    <p>POR reserves the right to read all messages of any type that are sent across our network.</p><br>

    <p>POR reserves the right to change these rules with or without notice.</p></center>
</div><br><br><br><br><br><br>
  


<?php include 'sections/footer.php'; ?>
