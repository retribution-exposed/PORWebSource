<?php include 'sections/header.php'; 

$key = $conn->real_escape_string($_REQUEST['k']);

if (strlen($key) !== 128) {
    header("location:index.php");
    exit(0);
}

$sql = "UPDATE Tab_Reg SET Verified='1' WHERE Verified='" . $key . "'";
$conn->query($sql) or die ('Something has gone wrong, try again later');

if ($conn->affected_rows === 0) {
    header("location:index.php");
    exit(0);
}

?>
<div class="page_center">
    <h1>Verified</h1>
    <br><br>
    <p>
        Avast! Yer account has been verified! See ye on the High Seas!

    </p>
</div>
<?php include 'sections/footer.php';?>